from django.test import TestCase
from django.urls import resolve, reverse
from .views import index, search
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story8UnitTest(TestCase):
	def test_url_index_exists(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_using_landing_page_template(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'ajax/booklist.html')

	def test_url_search_exists(self):
		url = reverse('search', args=['outlier'])
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)

	def test_using_search_function(self):
		url = reverse('search', args=['outlier'])
		found = resolve(url)
		self.assertEqual(found.func, search)

	def test_title_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Bookle', html_response)

	def test_search_box_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<input type="text"', html_response)

	def test_search_button_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Search</button>', html_response)		

	def test_table_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<table class="table">', html_response)

	def test_button_has_onclick_attribute(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<button onclick="readInput()"', html_response)

	def test_jquery_script_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<script src="/static/ajax/js/jquery-3.4.1.min.js"', html_response)

	def test_other_js_script_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<script src="/static/ajax/js/story8.js"', html_response)

class Story8FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story8FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story8FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')

		search_box = selenium.find_element_by_id('input')
		button = selenium.find_element_by_class_name('btn')
		time.sleep(5)
		
		selenium.execute_script("window.scrollTo(0, 400)")
		time.sleep(5)
		selenium.execute_script("window.scrollTo(400, 1000)")
		time.sleep(5)
		selenium.execute_script("window.scrollTo(1000, 1600)")
		time.sleep(5)

		search_box.send_keys('Outlier')
		time.sleep(3)

		button.click()
		time.sleep(5)
		selenium.execute_script("window.scrollTo(0, 500)")
		time.sleep(5)

		search_box.send_keys('Search')
		time.sleep(3)

		button.click()
		time.sleep(5)
		selenium.execute_script("window.scrollTo(0, 400)")
		time.sleep(5)
		selenium.execute_script("window.scrollTo(400, 900)")
		time.sleep(5)
		selenium.execute_script("window.scrollTo(900, 1400)")
		time.sleep(5)
		selenium.execute_script("window.scrollTo(1400, 1800)")
		time.sleep(5)