from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def index(request):
	return render(request, 'ajax/booklist.html')

def search(request, title):
	response = requests.get('https://www.googleapis.com/books/v1/volumes?q=intitle:' + title)
	return JsonResponse(response.json())	
